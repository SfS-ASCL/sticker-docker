This repository hosts Docker images of sticker models. For
more information, see:

* https://github.com/stickeritis/sticker/
* https://github.com/stickeritis/sticker-models/